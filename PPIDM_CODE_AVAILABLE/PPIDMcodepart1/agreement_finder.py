from global_variables import *
from headers import *


# cnx = MySQLdb.connect(
#         host='localhost', user='USERNAME', passwd='PASSWORD', db='DBNAME',
#         cursorclass=MySQLdb.cursors.SSCursor) # put the cursorclass here
# cursor = cnx.cursor()

def taxdomain_from_uniprot(DB, domain='no', tax='no'):
    start = datetime.datetime.now()
    print("GO-Seq, Domain-Seq, Tax-Seq for %s from %s (around 30 secs OR 17 mins on feynman)" % (domain, DB))

    if not tax == 'no':
        result_taxonomy = open(result_address + 'tax-seq-' + DB + '.txt', 'w')

    if not domain == 'no':
        result_domain = open(result_address + domain + '-seq-' + DB + '.txt', 'w')

    whichDB = ''
    if DB == 'sp':
        whichDB = 'sprot'
    elif DB == 'tr':
        whichDB = 'trembl'
    file1 = open(source_address + 'uniprot_' + whichDB + '.dat', 'r')

    Count1 = 0
    Count2 = 0
    counterAcc = 0

    # Reading line by line from bigDataAddress and parse the file. If it is useful, keep it.
    # Be careful of changes in the input file template.
    counter = 0
    for line1 in file1:
        if "ID   " in line1 and "eviewed" in line1:
            counter += 1
            ACCESSION = ""
            TAXID = ""
            TAXList = ""
            GO_TERM = ""
            DOM = ""
            ANNOTAION = ""
            for line1 in file1:
                if "AC" in line1 and line1.index('AC') == 0 and ACCESSION == "":
                    counterAcc += 1
                    line1Split = re.split('   ', line1.rstrip())
                    ACCESSION = line1Split[1].split(";")

                if not tax == 'no':
                    if "OS" in line1 and line1.index('OS') == 0:
                        line1Split = re.split('   ', line1.rstrip())
                        TAXID = line1Split[1]
                        #
                    if "OC" in line1 and line1.index('OC') == 0:
                        line1Split = re.split('   ', line1.rstrip())
                        TAXList += line1Split[1]

                if not domain == 'no':
                    if domain == 'cath':
                        keyword = 'Gene3D;'
                    elif domain == 'scop':
                        keyword = 'SUPFAM;'
                    elif domain == 'tigrfam':
                        keyword = 'TIGRFAMs;'
                    elif domain == 'pfam':
                        keyword = 'Pfam;'

                    elif domain == 'smart':
                        keyword = 'SMART;'
                    elif domain == 'prosite':
                        keyword = 'PROSITE;'
                    elif domain == 'panther':
                        keyword = 'PANTHER;'
                    elif domain == 'interpro':
                        keyword = 'InterPro;'

                    elif domain == 'prints':
                        keyword = 'PRINTS;'
                    elif domain == 'cdd':
                        keyword = ' CDD; '

                    elif domain == 'ALL':
                        keyword = 'ALLSHOULDBECONSIDERED'

                    if "DR" in line1 and line1.index('DR') == 0 and keyword in line1:
                        line1Split = re.split(';', line1.rstrip())
                        pf = line1Split[1]
                        DOM = pf + DOM
                        Count2 += 1

                    if "DR" in line1 and line1.index('DR') == 0 and keyword == 'ALLSHOULDBECONSIDERED':
                        if ' CDD; ' in line1 or 'PRINTS;' in line1 or 'InterPro;' in line1 or 'PANTHER;' in line1 or 'PROSITE;' in line1 or 'SMART;' in line1 or 'Pfam;' in line1 or 'TIGRFAMs;' in line1 or 'SUPFAM;' in line1 or 'Gene3D;' in line1:
                            line1Split = re.split(';', line1.rstrip())
                            pf = line1Split[1]
                            DOM = pf + DOM
                            Count2 += 1

                if "SQ" in line1 and line1.index('SQ') == 0 and "SEQUENCE" in line1:
                    break

            if not tax == 'no':
                if ACCESSION != "" and TAXID != "":
                    result_taxonomy.write(ACCESSION[0] + "\t" + TAXID + "\t" + TAXList + "\n")

            if not domain == 'no':
                if ACCESSION != "" and DOM != "":
                    DOMs = DOM.split(" ")
                    DOMs = set(DOMs)
                    DOMs = filter(None, DOMs)
                    for j in range(0, len(DOMs)):
                        dd = DOMs[j]
                        result_domain.write(dd + "\t" + ACCESSION[0] + "\n")

    if not tax == 'no':
        result_taxonomy.close()

    if not domain == 'no':
        result_domain.close()
        print('Number of Domain: ' + str(Count2))

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")




def domain_domain_agreement(domain, source):
    start = datetime.datetime.now()
    print("domain_domain_agreement for %s interaction in SwissProt(around ? mins)" %  domain)

    outputfile = result_address + "pfam-pfam-interaction-calculated"
    ddi_file = open(outputfile, "r")
    domain_domain = {}
    domain_domain_Score = {}
    for line in ddi_file:
        lineSplit = line.split("\t")
        dom1 = lineSplit[0]
        dom2 = lineSplit[1]
#         if domain == 'scop':
#             domam = domam[3:]
        score = lineSplit[2].rstrip()
        if dom1 in domain_domain:
            domain_domain[dom1].append(dom2)
        else:
            domain_domain[dom1] = []
            domain_domain[dom1].append(dom2)

        if dom2 in domain_domain:
            domain_domain[dom2].append(dom1)
        else:
            domain_domain[dom2] = []
            domain_domain[dom2].append(dom1)
        domain_domain_Score[(dom1,dom2)] = score
    
    print(len(domain_domain))
    print(len(domain_domain_Score))

    outputfile = result_address + source
    PPI_file = open(outputfile, "r")

    shared_interaction_doms = {}
    interaction_dom = {}
    PPI = set()
    PPI_dict = dict()
    all_seqs = set()
    for line in PPI_file:
        line_sp = line.rstrip().split("\t")
        seq1 = line_sp[0]
        seq2 = line_sp[1]
        all_seqs.add(seq1)
        all_seqs.add(seq2)
        PPI.add((seq1, seq2))
        if seq1 in PPI_dict:
            PPI_dict[seq1].add(seq2)
        else:
            PPI_dict[seq1] = set()
            PPI_dict[seq1].add(seq2)

        if seq2 in PPI_dict:
            PPI_dict[seq2].add(seq1)
        else:
            PPI_dict[seq2] = set()
            PPI_dict[seq2].add(seq1)

    outputfile = result_address + "tax-seq-sp.txt"
    taxonomySeqFile = open(outputfile, "r")
    
    IDtax = {}
    seqID = {}
    seqtax = {}
    for line in taxonomySeqFile:
        lineSP = line.rstrip().split('\t')
        seq = lineSP[0]
        if seq not in all_seqs:
            continue
        tax = lineSP[2]
        taxSP = tax.split(';')
        temp  = ""
        for item in taxSP:
            if item.endswith("."):
                item = item[:-1]
            temp = temp + (item) + ";"
            if seq in seqtax:
                seqtax[seq].add(temp)
            else:
                seqtax[seq] = set()
                seqtax[seq].add(temp)

    outputfile = result_address + "tax-seq-tr.txt"
    taxonomySeqFile = open(outputfile, "r")

    for line in taxonomySeqFile:
        lineSP = line.rstrip().split('\t')
        seq = lineSP[0]
        if seq not in all_seqs:
            continue
        tax = lineSP[2]
        taxSP = tax.split(';')
        temp = ""
        for item in taxSP:
            if item.endswith("."):
                item = item[:-1]
            temp = temp + (item) + ";"
            if seq in seqtax:
                seqtax[seq].add(temp)
            else:
                seqtax[seq] = set()
                seqtax[seq].add(temp)

    print('tax reading: OK ')

    outputfile = result_address + "pfam-seq-sp"
    pfamseqfile = open(outputfile, "r")
    list_dom_by_seq = {}
    list_seq_by_dom = {}
    for line in pfamseqfile:
        line_sp = line.rstrip().split('\t')
        seq = line_sp[1]
        if seq not in all_seqs:
            continue
        dom = line_sp[0]
        if seq  in list_dom_by_seq:
            list_dom_by_seq[seq].add(dom)
        else:
            list_dom_by_seq[seq] = set()
            list_dom_by_seq[seq].add(dom)

        if dom in list_seq_by_dom:
            list_seq_by_dom[dom].add(seq)
        else:
            list_seq_by_dom[dom] = set()
            list_seq_by_dom[dom].add(seq)

    print(len(list_dom_by_seq))
    print(len(list_seq_by_dom))
    outputfile = result_address + "pfam-seq-tr"
    pfamseqfile = open(outputfile, "r")
    for line in pfamseqfile:
        line_sp = line.rstrip().split('\t')
        seq = line_sp[1]
        if seq not in all_seqs:
            continue
        dom = line_sp[0]
        if seq in list_dom_by_seq:
            list_dom_by_seq[seq].add(dom)
        else:
            list_dom_by_seq[seq] = set()
            list_dom_by_seq[seq].add(dom)

        if dom in list_seq_by_dom:
            list_seq_by_dom[dom].add(seq)
        else:
            list_seq_by_dom[dom] = set()
            list_seq_by_dom[dom].add(seq)


    all_taxa = set()
    for seq in all_seqs:
        if seq not in seqtax:
            continue
        all_taxa.union(seqtax[seq])

    counter = 0
    outputfile = result_address + "pfam-pfam-interaction-calculated-taxa-" + source[8:] + ".txt"
    result_recall_precision = open(outputfile, 'w')
    for (dom1, dom2) in domain_domain_Score:
        counter += 1
        print(counter)
        # if domain_domain_Score[(dom1, dom2)] != str(1.0):
        #     continue
        if dom1 not in list_seq_by_dom or dom2 not in list_seq_by_dom:
            continue
        counter_ddi_to_ppi_nominator = dict()
        counter_ddi_to_ppi_denominator = dict()
        seq_list1 = list_seq_by_dom[dom1] & all_seqs & set(seqtax.keys())
        seq_list2 = list_seq_by_dom[dom2] & all_seqs & set(seqtax.keys())
        if len(seq_list1) == 0:
            for seq2 in seq_list2:
                taxa2 = seqtax[seq2]
                for taxon1 in all_taxa:
                    for taxon2 in taxa2:
                        taxon = (taxon1, taxon2)
                        if taxon in counter_ddi_to_ppi_denominator:
                            counter_ddi_to_ppi_denominator[taxon] += 1
                        else:
                            counter_ddi_to_ppi_denominator[taxon] = 1

        if len(seq_list2) == 0:
            for seq1 in seq_list1:
                taxa1 = seqtax[seq1]
                for taxon1 in taxa1:
                    for taxon2 in all_taxa:
                        taxon = (taxon1, taxon2)
                        if taxon in counter_ddi_to_ppi_denominator:
                            counter_ddi_to_ppi_denominator[taxon] += 1
                        else:
                            counter_ddi_to_ppi_denominator[taxon] = 1

        for seq1 in seq_list1:
            taxa1 = seqtax[seq1]
            for seq2 in seq_list2:
                taxa2 = seqtax[seq2]
                if (seq1, seq2) in PPI or (seq2, seq1) in PPI:
                    for taxon1 in taxa1:
                        for taxon2 in taxa2:
                            taxon = (taxon1, taxon2)

                            if taxon in counter_ddi_to_ppi_nominator:
                                counter_ddi_to_ppi_nominator[taxon] += 1
                            else:
                                counter_ddi_to_ppi_nominator[taxon] = 1

                            if taxon in counter_ddi_to_ppi_denominator:
                                counter_ddi_to_ppi_denominator[taxon] += 1
                            else:
                                counter_ddi_to_ppi_denominator[taxon] = 1

                if seq1 in PPI_dict:
                    if seq2 not in PPI_dict[seq1]:
                        for taxon1 in taxa1:
                            for taxon2 in taxa2:
                                taxon = (taxon1, taxon2)

                                if taxon in counter_ddi_to_ppi_denominator:
                                    counter_ddi_to_ppi_denominator[taxon] += 1
                                else:
                                    counter_ddi_to_ppi_denominator[taxon] = 1

                if seq2 in PPI_dict:
                    if seq1 not in PPI_dict[seq2]:
                        for taxon1 in taxa1:
                            for taxon2 in taxa2:
                                taxon = (taxon1, taxon2)

                                if taxon in counter_ddi_to_ppi_denominator:
                                    counter_ddi_to_ppi_denominator[taxon] += 1
                                else:
                                    counter_ddi_to_ppi_denominator[taxon] = 1


        # print(dom1, dom2, domain_domain_Score[(dom1, dom2)])
        for taxon in counter_ddi_to_ppi_denominator:
            confidence = 'NA'
            nom = 'NA'
            denom = 'NA'
            if counter_ddi_to_ppi_denominator[taxon] != 0:
                if taxon in counter_ddi_to_ppi_nominator:
                    nom = counter_ddi_to_ppi_nominator[taxon]
                else:
                    nom = 0

                denom = counter_ddi_to_ppi_denominator[taxon]

                confidence = float(nom)/denom

            if nom != 0 and confidence != 'NA':
                result_recall_precision.write(dom1 + '\t' + dom2 + '\t' + str(domain_domain_Score[(dom1, dom2)]) + '\t' + taxon[0] + '\t' + taxon[1] + '\t' + str(nom) + '\t' + str(denom) + '\t' + str(confidence) + '\n')

            # if confidence != 1.0:
            #     print(taxon)
            #     print(confidence)
            #     raw_input()


    #     for (dom1, dom2) in domain_domain_Score:
    #         if (dom1 in list_dom_by_seq[seq1] and dom2 in list_dom_by_seq[seq2]) or (dom1 in list_dom_by_seq[seq2] and dom2 in list_dom_by_seq[seq1]):
    #             if (dom1, dom2) in shared_interaction_doms:
    #                 shared_interaction_doms[(dom1, dom2)] += 1
    #             else:
    #                 shared_interaction_doms[(dom1, dom2)] = 1
    #         if dom1 != dom2:
    #             if dom1 in list_dom_by_seq[seq1] or dom1 in list_dom_by_seq[seq2]:
    #                 if dom1 in interaction_dom:
    #                     interaction_dom[dom1] += 1
    #                 else:
    #                     interaction_dom[dom1] = 1
    #             if dom2 in list_dom_by_seq[seq1] or dom2 in list_dom_by_seq[seq2]:
    #                 if dom2 in interaction_dom:
    #                     interaction_dom[dom2] += 1
    #                 else:
    #                     interaction_dom[dom2] = 1
    #         else:
    #             if dom1 in list_dom_by_seq[seq1] or dom1 in list_dom_by_seq[seq2]:
    #                 if dom1 in interaction_dom:
    #                     interaction_dom[dom1] += 1
    #                 else:
    #                     interaction_dom[dom1] = 1
    #
    # for (dom1,dom2) in shared_interaction_doms:







#     outputfile = result_address + "pfam-pfam-interaction-calculated-taxa.txt"
#     result_recall_precision = open(outputfile, 'w')
#
#     agreement_SP_Up = {}
#     agreement_SP_Down = {}
#     lineCount = 0
#     alreadyExist = 0
#     outputfile = result_address +  domain + "-seq-sp.txt"
#     domainSeqFile = open(outputfile, "r")
#     domainSeq = {}
#     for line in domainSeqFile:
#         lineSplit = line.split("\t")
#         dom = lineSplit[0]
#         seq = lineSplit[1].rstrip()
#
#         if not seq in seqtax:
#             continue
#         taxs = seqtax[seq]
#
#         if dom in domainSeq:
#             for tax in taxs:
#                 if tax in domainSeq[dom]:
#                     domainSeq[dom][tax].append(seq)
#                 else:
#                     domainSeq[dom][tax] = []
#                     domainSeq[dom][tax].append(seq)
#         else:
#             domainSeq[dom] = {}
#             for tax in taxs:
#                 domainSeq[dom][tax] = []
#                 domainSeq[dom][tax].append(seq)
#
#     finalDict = {}
#     counter = 0
#     for dom in domainGO:
#         counter += 1
#         print(counter)
#         for go in domainGO[dom]:
#             if not go in goSeqList:
#                 continue
#             associations = []
#             taxFPzero = set()
#             taxlist = goSeqList[go].keys()
#             taxlist = sorted(taxlist)
#             for tax in taxlist:
#                 if tax in domainSeq[dom]:
#                     for item in taxFPzero:
#                         if item in tax:
#                             continue
#
#                     goSEQs = goSeqList[go][tax]
#                     domSEQs = domainSeq[dom][tax]
#
#                     TP = set(domSEQs) & set(goSEQs)
#                     FP = set(domSEQs) - set(goSEQs)
#                     FN = set(goSEQs) - set(domSEQs)
#
#                     TPR = float(len(TP)) / (len(TP) + len(FN))
#                     PPV = float(len(TP)) / (len(TP) + len(FP))
#                     if len(TP) != 0:
#                         line = go + "\t" + dom + "\t" + str(domainGOScore[(go,dom)]) + "\t" + str(len(TP)) + "\t" + str(len(FP)) + "\t" + str(len(FN)) + "\t" + str(TPR) + "\t" + str(PPV) + "\t" + tax + "\n"
#                         associations.append(line)
#                         if len(FP) == 0:
#                             taxFPzero.add(tax)
#             for line in associations:
#                 result_recall_precision.write(line)
#
# #                     for seq in TP:
# #                         finalDict[(go, seq)] = 'InSwissProt'
# # #                         result.write(go + "\t" + seq + "\t" + dom + "\tInSwissProt" + "\n")
# #
# #                     for seq in FP:
# #                         if not (go, seq) in finalDict:
# #                             finalDict[(go, seq)] = 'New'
# #                         result.write(go + "\t" + seq + "\t" + dom + "\tNew" + "\n")
#
# #                     for seq in FN:
# #                         result.write(go + "\t" + seq + "\t" + dom + "\tCantFind" + "\n")
#
#     # print(len(finalDict))
#
#
#
    result_recall_precision.close()
    
    












