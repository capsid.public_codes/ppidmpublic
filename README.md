# ppidm2020

PPIDM description : The PPIDM  (Protein-Protein Interactions Domain Miner) approach has been designed by Zia Alborzi during his PhD thesis in the Capsid team. The goal is to infer Domain-Domain interactions (DDIs) from multiple sources of PPIs.   
The approach is an extension of our previously described CODAC (Computational Discovery of Direct Associations using Common neighbors) method for inferring new edges in a tripartite graph. 
The PPIDM method has been applied to seven widely used PPI resources, using as Gold-Standard a set of DDIs extracted from 3D structural databases. Overall, PPIDM has produced a dataset of 84,552 non-redundant DDIs.

References : 
CODAC : Alborzi SZ, Ritchie DW, Devignes MD. [Computational discovery of direct associations between GO terms and protein domains.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6245584/pdf/12859_2018_Article_2380.pdf) BMC Bioinformatics 19-S(14):53-66. doi:10.1186/s12859-018-2380-2 (2018).  
PPIDM : Alborzi SZ, Nacer AA, Najjar H, Ritchie DW, Devignes MD. [PPIDomainMiner : Inferring domain-domain interactions from multiple sources of protein-protein interactions.](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1008844) PLOS Computational Biology 17(8):e1008844. doi:10.1371/journal.pcbi.1008844 (2021) . 

List of codes necessary to run PPIDM.  
Contributors : Zia Alborzi, Amina Ahmed-Nacer, Hiba Najjar

Input data: prepared PPI files from various sources. Available from [zenodo](https://zenodo.org/records/4880347).

Output data: csv file with headers : Domain 1, Domain 2, Consensus Score, [Score in each PPI source, P-Value in each PPI source] repeated, Category, In_Gold-Standard (yes/no). Available from [zenodo doi:10.5281/zenodo.4880347](https://zenodo.org/records/4880347)  and in part from [PPIDM web page](https://mbi-ds4h.loria.fr/ppidomainminer/) .

The code is divided in three parts according to different authors and steps in the PPIDM process.

ppidmcodepart1 (Zia Alborzi) : Computation of the PPIDM consensus similarity matrix.
    For each PPI source, produce adjacency matrices, compute cosine similarity and p-values. Optimize the set of weights and compute the consensus similarity matrix. This part is constituted of python scripts.

ppidmcodepart2 (Hiba Najjar) : Generating the PPIDM dataset and categorization.
    Determine the threshold, produce the final list of PPIs, assign to categories, compare with existing datasets (3did, domine). This part is constituted of python scripts embedded in jupyter notebooks.

ppidmcodepart3 (Amina Ahmed nacer) : Evaluating the PPIDM dataset.
    Determining relevant DDIs for explaining PPIs. This part is constituted of java programs.



