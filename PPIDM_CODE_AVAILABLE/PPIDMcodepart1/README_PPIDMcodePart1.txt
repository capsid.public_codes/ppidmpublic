#Computation of the PPIDM consensus similarity matrix.
This README file explains the python code organisation for PPIDMcodePart1
Written by Zia Alborzi, coauthor of publication ref https://biorxiv.org/cgi/content/short/2021.03.03.433732


The sequence of scripts is described in the main.py script:

from interaction_clear_3did_kbdock import *

from read_input_files import *

from process_tables import *

from filtering import *

from pvalue import *

from interaction_go_verification import*

from agreement_finder import *

