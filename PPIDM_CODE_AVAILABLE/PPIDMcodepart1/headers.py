import MySQLdb
import MySQLdb.cursors
import sys
import operator
import random
import os
import re
import datetime
import math
from math import factorial
from math import sqrt
import copy
import itertools
from collections import Counter
from itertools import chain, combinations
import urllib2
import gzip


cnx = MySQLdb.connect(
    host='localhost', user='USERNAME', passwd='PASSWORD', db='DBNAME',
    cursorclass=MySQLdb.cursors.SSCursor)
cursor = cnx.cursor()