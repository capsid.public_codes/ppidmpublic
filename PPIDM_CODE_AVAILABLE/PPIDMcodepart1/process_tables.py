from global_variables import *
from headers import *


def read_from_database(all_from_table, InfoSimilarity):
    info_dict = {}
    for item in all_from_table:
        print(len(info_dict))
        # if not item[0] == '1.4.3.2' and not item[0] =='PF01266':
        #     continue
        item_type1 = item[0]
        acc_type1 = item[1]
        if acc_type1 in InfoSimilarity:
            acc_cluster = InfoSimilarity[acc_type1]
        else:
            acc_cluster = acc_type1

        # acc_cluster = hash(acc_cluster)
        if item_type1 in info_dict:
            info_dict[item_type1].add(acc_cluster)
        else:
            info_dict[item_type1] = set()
            info_dict[item_type1].add(acc_cluster)
        # if len(info_dict) == 100:
        #     break
    return info_dict



def similarity_calculator_interaction(source, domain):
    start = datetime.datetime.now()
    print("SIMILARITY Calculator for Interactions(around ? mins)")

    seqpdbchain = dict()
    pdbchainDom = dict()
    file1 = open(source_address + 'pdb_chain_pfam.tsv', 'r')
    file1.readline()
    file1.readline()
    for line in file1:
        line_sp = line.rstrip().split('\t')
        dom = line_sp[3]
        seq = line_sp[2]
        pdbchain = line_sp[0] + '_' + line_sp[1]
        if pdbchain in pdbchainDom:
            pdbchainDom[pdbchain].append(dom)
        else:
            pdbchainDom[pdbchain] = list()
            pdbchainDom[pdbchain].append(dom)

        if seq in seqpdbchain:
            seqpdbchain[seq].add(pdbchain)
        else:
            seqpdbchain[seq] = set()
            seqpdbchain[seq].add(pdbchain)

    seqDom = dict()
    file1 = open(result_address + 'pfam-seq-sp', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        dom = line_sp[0]
        seq = line_sp[1]
        if seq in seqDom:
            seqDom[seq].append(dom)
        else:
            seqDom[seq] = list()
            seqDom[seq].append(dom)

    file1 = open(result_address + 'pfam-seq-tr', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        dom = line_sp[0]
        seq = line_sp[1]
        if seq in seqDom:
            seqDom[seq].append(dom)
        else:
            seqDom[seq] = list()
            seqDom[seq].append(dom)

    result = open(result_address + source + 'pfam', 'w')
    file1 = open(result_address + source, 'r')
    # used_pair = set()
    # all_seq_A = dict()
    each_interaction_seq_seq = dict()
    interaction_dict = dict()

    for line in file1:
        if '_' in line:
            continue
        line_sp = line.rstrip().split('\t')
        item1 = line_sp[0]
        item2 = line_sp[1]

        # if (item2, item1) in used:
        #     continue
        domains1 = []
        domains2 = []

        if item1 in seqDom and item2 in seqDom:
            domains1 = seqDom[item1]
            domains2 = seqDom[item2]
            if item1 in seqpdbchain and item2 in seqpdbchain:
                pdbchains1 = seqpdbchain[item1]
                pdbchains2 = seqpdbchain[item2]
                for pc in pdbchains1:
                    domains1 = domains1 + pdbchainDom[pc]
                for pc in pdbchains2:
                    domains2 = domains2 + pdbchainDom[pc]

        else:
            if item1 in seqpdbchain and item2 in seqpdbchain:
                pdbchains1 = seqpdbchain[item1]
                pdbchains2 = seqpdbchain[item2]
                for pc in pdbchains1:
                    domains1 = domains1 + pdbchainDom[pc]
                for pc in pdbchains2:
                    domains2 = domains2 + pdbchainDom[pc]

            else:
                continue


        domains1 = list(set(domains1))
        domains2 = list(set(domains2))

        subsets1 = chain(*map(lambda x: combinations(domains1, x), range(1, 3)))
        subsets2 = chain(*map(lambda x: combinations(domains2, x), range(1, 3)))
        subsets1 = list(subsets1)
        subsets2 = list(subsets2)

        # print(domains1)
        # print(domains2)
        #
        # print(subsets1)
        # print(subsets2)
        # exit()
        if item1 < item2:
            seq_seq = item1 + '_' + item2
        else:
            seq_seq = item2 + '_' + item1

        for set1 in subsets1:
            for set2 in subsets2:
                interaction1 = ''
                interaction2 = ''
                interaction = ()
                set1 = set(set1)
                set2 = set(set2)
                for datum in set1:
                    interaction1 += datum + '_'
                for datum in set2:
                    interaction2 += datum + '_'

                # print(interaction1)
                # print(interaction2)
                # raw_input()

                if interaction1 < interaction2:
                    interaction = (interaction1[:-1],interaction2[:-1])
                else:
                    interaction = (interaction2[:-1], interaction1[:-1])

                result.write(str(interaction[0]) + '\t' + seq_seq + '\t' + str(interaction[1]) + '\n')

                if interaction[0] in each_interaction_seq_seq:
                    each_interaction_seq_seq[interaction[0]].add(seq_seq)
                else:
                    each_interaction_seq_seq[interaction[0]] = set()
                    each_interaction_seq_seq[interaction[0]].add(seq_seq)

                if interaction[1] in each_interaction_seq_seq:
                    each_interaction_seq_seq[interaction[1]].add(seq_seq)
                else:
                    each_interaction_seq_seq[interaction[1]] = set()
                    each_interaction_seq_seq[interaction[1]].add(seq_seq)

                if interaction in interaction_dict:
                    interaction_dict[interaction].add(seq_seq)
                else:
                    interaction_dict[interaction] = set()
                    interaction_dict[interaction].add(seq_seq)

        # A_list = set()
        # for set1 in subsets1:
        #     set1 = sorted(set1)
        #     A = ""
        #     for element in set1:
        #         A = A + element + "-"
        #
        #     A_list.add(A)
        #     if A in all_seq_A:
        #         all_seq_A[A].add(seq_seq)
        #     else:
        #         all_seq_A[A] = set()
        #         all_seq_A[A].add(seq_seq)
        #
        # B_list = set()
        # for set2 in subsets2:
        #     set2 = sorted(set2)
        #     B = ""
        #     for element in set2:
        #         B = B + element + "-"
        #
        #     B_list.add(B)
        #     if B in all_seq_B:
        #         all_seq_B[B].add(seq_seq)
        #     else:
        #         all_seq_B[B] = set()
        #         all_seq_B[B].add(seq_seq)

        # for A in A_list:
        #     for B in B_list:
        #         used_pair.add((A,B))
        #         used_pair.add((B,A))

    result.close()

    result_file = open(result_address + domain + '-' + domain  + '-interaction-'+ source[8:] , 'w')
    # counter = 0
    for interaction in interaction_dict:
        # counter += 1
        # print(counter)
        interctor1 = interaction[0]
        interctor2 = interaction[1]
        intersection = interaction_dict[interaction]
        nominator = len(intersection)
        denom1 = len(each_interaction_seq_seq[interctor1])
        denom2 = len(each_interaction_seq_seq[interctor2])

        similarity = float(nominator) / (sqrt(denom1) * sqrt(denom2))
        # if similarity != 0:
            # print(item1)
            # print(item2)
        result_file.write(str(interctor1) + '\t' + str(interctor2) + '\t' + str(denom1) + '\t' + str(denom2) + '\t' + str(nominator) + '\t' + str(similarity) + '\n')
    result_file.close()
    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")
