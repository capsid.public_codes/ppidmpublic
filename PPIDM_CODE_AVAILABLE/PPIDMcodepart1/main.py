
from interaction_clear_3did_kbdock import *
from read_input_files import *
from process_tables import *
from filtering import *
from pvalue import *
from interaction_go_verification import*
from agreement_finder import *

# create_table_3did_kbdock_domain()

# intact_reader()
# mint_reader()
# dip_reader()
# uniprot_tags_reader('sp')
# biogrid_reader()
# string_reader()
# sifts_accession_reader()
# hprd_reader()

# similarity_calculator_interaction('source1_intact', 'pfam')
# similarity_calculator_interaction('source2_mint', 'pfam')
# similarity_calculator_interaction('source3_dip', 'pfam')
# similarity_calculator_interaction('source4_biogrid', 'pfam')
# similarity_calculator_interaction('source5_string-exp', 'pfam')
# similarity_calculator_interaction('source5_string-rest', 'pfam')
# similarity_calculator_interaction('source6_sifts_accession', 'pfam')
# similarity_calculator_interaction('source7_hprd', 'pfam' )
##### sifts_reader_process('sifts', 'pfam')

# create_wrong_assocations()

# assign_interaction()

# intersection_CAPS_3did_kbdock()
# kbdock_union_3did()

# pvalue_calculation('source1_intact')
# pvalue_calculation('source2_mint')
# pvalue_calculation('source3_dip')
# pvalue_calculation('source7_hprd')
# pvalue_calculation('source4_biogrid')
# pvalue_calculation('source5_string-exp')
# pvalue_calculation('source5_string-rest')
# pvalue_calculation('source6_sifts_accession')
# accumulate_pvalues()
# gold_silver_bronze()

# checkOneOnedomain()

# verify_go_for_each_pair()

# one_to_one()
#---------------------------------------

# taxdomain_from_uniprot('sp', 'no', 'yes')
# taxdomain_from_uniprot('tr', 'no', 'yes')

# domain_domain_agreement('pfam', 'source1_intact')
# domain_domain_agreement('pfam', 'source2_mint')
# domain_domain_agreement('pfam', 'source3_dip')
# domain_domain_agreement('pfam', 'source4_biogrid')
# domain_domain_agreement('pfam', 'source7_hprd')
# domain_domain_agreement('pfam', 'source5_string-exp')
# domain_domain_agreement('pfam', 'source5_string-rest')

