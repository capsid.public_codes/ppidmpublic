import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class PPIwithPfam {

	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);

		String ppi = "ppiList";
		String pfams = "proteinPfam";
		System.out.println("Souhaitez-vous d�finir le nom du fichier des PPIs ? (y/n) : ");
		if ((sc.nextLine()).equals("y")) {
			System.out.println("Nom du fichier contenant les PPIs (sans l'extension) :");
			ppi = sc.nextLine();

		}
		System.out.println("Souhaitez-vous d�finir le nom du fichier des DDIs ? (y/n) : ");
		if ((sc.nextLine()).equals("y")) {
			System.out.println("Nom du fichier contenant les DDIs (sans l'extension) :");
			ppi = sc.nextLine();
		}

		doJob(ppi, pfams);
		sc.close();
	}

	public static void doJob(String ppiList, String pfamOfpdb) throws IOException {

		List<String> pfamsPDB = new ArrayList<String>(20500); 

		String line;
		

		BufferedReader br = new BufferedReader(new FileReader(new File(pfamOfpdb + ".csv")));
		while ((line = br.readLine()) != null) {
			pfamsPDB.add(line); 
							
		}
		System.out.println("pfamsPDB" + pfamsPDB.get(0));
		br.close();

		String[] ppi;
		PrintWriter writeRes = new PrintWriter(new File("PPIHavingPfam.txt"), "UTF-8");

		BufferedReader br2 = new BufferedReader(new FileReader(new File(ppiList + ".txt")));

		while ((line = br2.readLine()) != null) {
			List<String> pfamspdb0 = new ArrayList<String>(200);
																
			List<String> pfamspdb1 = new ArrayList<String>(200);
			String[] temp;
			ppi = line.split(","); 
			for (int i = 0; i < pfamsPDB.size(); i++) {
				temp = (pfamsPDB.get(i)).split(";");
				if (temp[0].equals(ppi[0]))
					for (int j = 0; j < temp.length; j++)
						pfamspdb0.add(temp[j]);
				if (temp[0].equals(ppi[1]))
					for (int j = 0; j < temp.length; j++)
						pfamspdb1.add(temp[j]);
			}

			
			if ((!(pfamspdb0.get(1)).equals("null")) && (!(pfamspdb1.get(1)).equals("null"))) // if true means one pdb
																								// has no pfam so score
																								// will be 0
			{
				writeRes.println(ppi[0] + "\t" + ppi[1]);

			}

		}

		writeRes.close();
		br2.close();
	}

}

