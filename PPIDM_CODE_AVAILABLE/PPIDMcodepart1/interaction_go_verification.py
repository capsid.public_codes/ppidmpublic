from global_variables import *
from headers import *


def verify_go_for_each_pair():
    start = datetime.datetime.now()
    print("verify_go_for_each_pair (around 2 secs)")

    interactions_3did = set()
    pfam_3did = set()
    file1 = open(result_address + '3did', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        item1 = line_sp[0]
        item2 = line_sp[1]
        if item1 < item2:
            interactions_3did.add((item1, item2))
        else:
            interactions_3did.add((item2, item1))
        pfam_3did.add(item1)
        pfam_3did.add(item2)

    interactions_kbdock = set()
    pfam_kbdock = set()
    file1 = open(result_address + 'kbdock', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        item1 = line_sp[0]
        item2 = line_sp[1]
        if item1 < item2:
            interactions_kbdock.add((item1, item2))
        else:
            interactions_kbdock.add((item2, item1))
        pfam_kbdock.add(item1)
        pfam_kbdock.add(item2)

    caps_score = dict()
    caps = set()
    pfam_caps = set()
    file1 = open(result_address + 'pfam-pfam-interaction-merged', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        if line_sp[1] > line_sp[0]:
            caps.add((line_sp[0], line_sp[1]))
            caps_score[(line_sp[0], line_sp[1])] = line_sp[9]
        else:
            caps.add((line_sp[1], line_sp[0]))
            caps_score[(line_sp[1], line_sp[0])] = line_sp[9]

        pfam_caps.add(line_sp[0])
        pfam_caps.add(line_sp[1])

    gold_standard = (interactions_kbdock & interactions_3did) & caps

    print(len(gold_standard))

    caps_score = dict()
    caps = set()
    pfam_caps = set()
    file1 = open(result_address + 'pfam-pfam-interaction-calculated', 'r')
    file1.readline()
    for line in file1:
        line_sp = line.rstrip().split('\t')
        if line_sp[1] > line_sp[0]:
            caps.add((line_sp[0], line_sp[1]))
            caps_score[(line_sp[0], line_sp[1])] = line_sp[9]
        else:
            caps.add((line_sp[1], line_sp[0]))
            caps_score[(line_sp[1], line_sp[0])] = line_sp[9]

        pfam_caps.add(line_sp[0])
        pfam_caps.add(line_sp[1])

    domain_go = dict()
    file1 = open(result_address + 'go_bp_pfam', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        go = line_sp[0]
        domain = line_sp[1]
        if domain in domain_go:
            domain_go[domain].add(go)
        else:
            domain_go[domain] = set()
            domain_go[domain].add(go)

    counter_correct = 0
    counter_not_correct = 0
    no_availble_dom1 = list()
    no_availble_dom2 = list()
    no_availble_domboth = 0
    for (dom1, dom2) in gold_standard:
        go_dom1 = set()
        go_dom2 = set()
        if dom1 in domain_go:
            go_dom1 = domain_go[dom1]
        else:
            no_availble_dom1.append(dom1)
        if dom2 in domain_go:
            go_dom2 = domain_go[dom2]
        else:
            no_availble_dom2.append(dom2)

        # if dom1 not in domain_go or dom2 not in domain_go:
        #     no_availble_domboth += 1

        intersection_gos = go_dom1 & go_dom2
        if dom1 in domain_go and dom2 in domain_go:
            if len(intersection_gos) > 0:
                counter_correct += 1
            else:
                counter_not_correct += 1


    print(len(no_availble_dom1))
    print(len(no_availble_dom2))
    print(no_availble_domboth)
    print(counter_correct)
    print(counter_not_correct)




    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")