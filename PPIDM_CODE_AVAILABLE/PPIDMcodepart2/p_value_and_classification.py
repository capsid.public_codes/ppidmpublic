

# P_VALUE CALCULATION

##############################################################################
# Import Libraries
##############################################################################

import datetime
import math
from math import sqrt
import pickle
import sys
import time




##############################################################################
# Path to useful directories and files
##############################################################################

source_address = "./Inputs/"
result_address = "./Results/"
sources = ["source1_intact", "source2_mint", "source3_dip", "source4_biogrid",
           "source5_string-exp", "source5_string-rest", "source6_sifts_accession",
           "source7_hprd"]
ppi = 'pfam_pfam_interaction_recalculated_84552'



##############################################################################
# Create the Sequence-Domains dictionary
##############################################################################

"""
seqDom = dict()
file1 = open(source_address + 'pfam-seq-sp', 'r')
print("Reading pfam-seq-sp :")
for line in file1:
    line_sp = line.rstrip().split('\t')
    dom = line_sp[0]
    seq = line_sp[1]
    if seq in seqDom:
        seqDom[seq].append(dom)
    else:
        seqDom[seq] = list()
        seqDom[seq].append(dom)
 
file1 = open(source_address + 'pfam-seq-tr', 'r')
print("Reading pfam-seq-tr :")
N = 65708449
i=0
for line in file1:
    i+=1
    if i%100000==0 : print(round(100*i/N, 2), '%')
    #if i>100000 : break
    line_sp = line.rstrip().split('\t')
    dom = line_sp[0]
    seq = line_sp[1]
    if seq in seqDom:
        seqDom[seq].append(dom)
    else:
        seqDom[seq] = list()
        seqDom[seq].append(dom)

# save dictionary:
with open(source_address+'seqDom_dictionnary', 'wb') as handle: pickle.dump(seqDom, handle, protocol=pickle.HIGHEST_PROTOCOL)

"""

# load dictionary
with open(source_address+'seqDom_dictionnary', 'rb') as handle: 
    seqDom = pickle.load(handle)




##############################################################################
# Definition of some helper functions
##############################################################################

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1


def p_value_cal_zia(N, Md, Ne, Kde):
    """ 
    p-value calculation make use of logarithmic calculus because of the 
    large values the arguments can take 
    """
    minMdNe = min(Ne, Md)

    coFactorNe = sqrt(2 * (math.pi) * Ne)
    coFactorMd = sqrt(2 * (math.pi) * Md)
    coFactorN = sqrt(2 * (math.pi) * N)
    NminusNe = N - Ne
    NminusMd = N - Md
    coFactorNminusNe = sqrt(2 * (math.pi) * NminusNe)
    coFactorNminusNMd = sqrt(2 * (math.pi) * NminusMd)
    headCoFactors = coFactorNe * coFactorMd * coFactorNminusNe * coFactorNminusNMd
    headCoFactors = math.log10(headCoFactors)

    logNe = math.log10(Ne / (math.e))
    logNe = logNe * Ne
    logMd = math.log10(Md / (math.e))
    logMd = logMd * Md
    logNMinusNe = math.log10(NminusNe / (math.e))
    logNMinusNe = logNMinusNe * NminusNe
    logNMinusMd = math.log10(NminusMd / (math.e))
    logNMinusMd = logNMinusMd * NminusMd
    headLog = logNe + logMd + logNMinusNe + logNMinusMd

    p_value = 0.0
    for i in range(Kde, minMdNe + 1):
        coFactorI = sqrt(2 * (math.pi) * i)
        NeMinusI = Ne - i
        MdMinusI = Md - i
        NMinusMdNePlusI = N - Md - Ne + i
        if NeMinusI == 0:
            coFactorNeMinusI = 1
        else:
            coFactorNeMinusI = sqrt(2 * (math.pi) * NeMinusI)

        if MdMinusI == 0:
            coFactorMdMinusI = 1
        else:
            coFactorMdMinusI = sqrt(2 * (math.pi) * MdMinusI)
        coFactorNMinusMdNePlusI = sqrt(2 * (math.pi) * NMinusMdNePlusI)

        tailCoFactor = coFactorNeMinusI * coFactorI * coFactorMdMinusI * coFactorNMinusMdNePlusI * coFactorN
        tailCoFactor = math.log10(tailCoFactor)

        logI = math.log10(i / (math.e))
        logI = logI * i
        if NeMinusI == 0:
            logNeMinusI = 1
        else:
            logNeMinusI = math.log10(NeMinusI / (math.e))
            logNeMinusI = logNeMinusI * NeMinusI
        if MdMinusI == 0:
            logMdMinusI = 1
        else:
            logMdMinusI = math.log10(MdMinusI / (math.e))
            logMdMinusI = logMdMinusI * MdMinusI
        logN = math.log10(N / (math.e))
        logN = logN * N
        logNMinusMdNePlusI = math.log10(NMinusMdNePlusI / (math.e))
        logNMinusMdNePlusI = logNMinusMdNePlusI * NMinusMdNePlusI

        tailLog = logI + logNeMinusI + logMdMinusI + logN + logNMinusMdNePlusI
        result = headLog + headCoFactors - tailLog - tailCoFactor
        result = 10 ** result
        p_value += result
    return p_value





def pvalue_calculation(source):
    global seqDom
    # writes in one file: 'newpvalue-' + source
    start = datetime.datetime.now()
    print("\n\nP-value calculation for ", source)

    N = 0

    ####################################
    print(source," - step1")
    pdbchainDom = dict()
    file1 = open(source_address + 'pdb_chain_pfam.tsv', 'r')
    file1.readline()
    file1.readline()
    for line in file1:
        line_sp = line.rstrip().split('\t')
        dom = line_sp[3]
        pdbchain = line_sp[0] + '_' + line_sp[1]
        if pdbchain in pdbchainDom:
            pdbchainDom[pdbchain].append(dom)
        else:
            pdbchainDom[pdbchain] = list()
            pdbchainDom[pdbchain].append(dom)
    file1.close()


    ####################################
    print("step2")
    allPPI = set()
    lengthfile = file_len(source_address + source)
    file1 = open(source_address + source, 'r')
    i=0
    for line in file1:
        i+=1
        if i%10000==0 : print(source," - step 2 - ", round(100*i/lengthfile, 2), '%')
        line_sp = line.rstrip().split('\t')
        item1 = line_sp[0]
        item2 = line_sp[1]
        if item1 < item2:
            seq_seq = item1 + '_' + item2
        else:
            seq_seq = item2 + '_' + item1

        allPPI.add(seq_seq)
    file1.close()

    N = len(allPPI)

    print("length of allPPI of", source, "is: ",N)

    ####################################
    print("step3")
    domain_seq_seq = dict()
    i=0

    allPPI_len = len(allPPI)
    for seq_seq in allPPI:
        i+=1
        
        if i%1000==0 : print(source," - step3 - ", round(100*i/allPPI_len, 2), '%')
        
        #if i>100 : break
        seq_split = seq_seq.split('_')
        if len(seq_split) > 2:
            seq1 = seq_split[0] + '_' + seq_split[1]
            seq2 = seq_split[2] + '_' + seq_split[3]
        else:
            seq1 = seq_split[0]
            seq2 = seq_split[1]
        if seq1 in seqDom:
            domains = seqDom[seq1]
            for domain in domains:
                if domain in domain_seq_seq:
                    domain_seq_seq[domain].add(seq_seq)
                else:
                    domain_seq_seq[domain] = set()
                    domain_seq_seq[domain].add(seq_seq)

        if seq2 in seqDom:
            domains = seqDom[seq2]
            for domain in domains:
                if domain in domain_seq_seq:
                    domain_seq_seq[domain].add(seq_seq)
                else:
                    domain_seq_seq[domain] = set()
                    domain_seq_seq[domain].add(seq_seq)

        if source == 'source6_sifts_accession':
            if seq1 in pdbchainDom:
                domains = pdbchainDom[seq1]
                for domain in domains:
                    if domain in domain_seq_seq:
                        domain_seq_seq[domain].add(seq_seq)
                    else:
                        domain_seq_seq[domain] = set()
                        domain_seq_seq[domain].add(seq_seq)

            if seq2 in pdbchainDom:
                domains = pdbchainDom[seq2]
                for domain in domains:
                    if domain in domain_seq_seq:
                        domain_seq_seq[domain].add(seq_seq)
                    else:
                        domain_seq_seq[domain] = set()
                        domain_seq_seq[domain].add(seq_seq)

    ####################################
    print("step4")
    final_result = open(result_address + 'newpvalue-' + source[8:], 'w')

    file1 = open(source_address + ppi, 'r')
    # file1.readline()
    i=0
    lengthfile = file_len(source_address + ppi)
    NA_p_val = 0
    null_K = 0
    null_score = 0
    etoile_p_val = 0
    t0 = time.time()
    etoile_p_val_list = []
    for line in file1:
        if i%1000==0 : 
          print(source," - step4 - ", round(100*i/lengthfile, 2), '% , and this took ', time.time() - t0, " seconds.")
          t0 = time.time()
        i+=1
        #if i>100 : break
        lineSplit = line.rstrip().split("\t")
        dom1 = lineSplit[0]
        dom2 = lineSplit[1]
        AssocScore = lineSplit[10]
        source_score = 0
        if source[8:] == 'intact':
            source_score = lineSplit[2]
        elif source[8:] == 'dip':
            source_score = lineSplit[3]
        elif source[8:] == 'mint':
            source_score = lineSplit[4]
        elif source[8:] == 'biogrid':
            source_score = lineSplit[5]
        elif source[8:] == 'string-exp':
            source_score = lineSplit[6]
        elif source[8:] == 'string-rest':
            source_score = lineSplit[7]
        elif source[8:] == 'sifts_accession':
            source_score = lineSplit[8]
        elif source[8:] == 'hprd':
            source_score = lineSplit[9]

        Nx = 0
        Ny = 0
        Kxy = 0
        if dom1 in domain_seq_seq:
            Nx = len(domain_seq_seq[dom1])
        if dom2 in domain_seq_seq:
            Ny = len(domain_seq_seq[dom2])
        if dom1 in domain_seq_seq and dom2 in domain_seq_seq:
            temp = domain_seq_seq[dom1] & domain_seq_seq[dom2]
            Kxy = len(temp)

        if Kxy == 0 or float(source_score) == 0.0:
            final_result.write(str(dom1) + "\t" + str(dom2) + "\t" + str(AssocScore) + "\t" + "NA" + "\n")
            NA_p_val +=1
            if Kxy==0: null_K +=1
            if float(source_score) == 0.0: null_score +=1
            continue
 
        p_value = p_value_cal_zia(N, Nx, Ny, Kxy)
        if p_value==None: print("N, Nx, Ny, Kxy = ",N, Nx, Ny, Kxy)
        if p_value > 1:
            etoile_p_val += 1
            etoile_p_val_list.append(p_value)
            p_value = "1*"
        final_result.write(str(dom1) + "\t" + str(dom2) + "\t" + str(AssocScore) + "\t" + str(p_value) + "\n")
        continue
    
    print("p_value greater than 1!:", etoile_p_val)
    print("Kxy null or score null:", NA_p_val)
    print("  - Kxy null:", null_K)
    print("  - score null:", null_score)
    
    ####################################
    print("step final")
    final_result.close()
    end = datetime.datetime.now()
    print("Running Time: " + str(end - start))
    print("END PROCESSING", source,'\n\n\n')
    
    return etoile_p_val_list




### Function accumulate_pvalues ###############################################

def accumulate_pvalues():
    intact_pvalue = dict()
    dip_pvalue = dict()
    mint_pvalue = dict()
    biogrid_pvalue = dict()
    string_exp_pvalue = dict()
    string_rest_pvalue = dict()
    sifts_pvalue = dict()
    hprd_pvalue = dict()
    caps_score = dict()

    file1 = open(source_address + ppi, 'r')
    for line in file1:
        line_sp = line.rstrip().split("\t")
        caps_score[(line_sp[0], line_sp[1])] = line_sp[10]

    file1 = open(result_address + 'newpvalue-intact', 'r')
    i=0
    for line in file1:
        i+=1
        line_sp = line.rstrip().split('\t')
        intact_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]
        # caps_score[(line_sp[0], line_sp[1])] = line_sp[2]
    print('intact dict:', i)

    file1 = open(result_address + 'newpvalue-dip', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        dip_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]

    file1 = open(result_address + 'newpvalue-mint', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        mint_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]

    file1 = open(result_address + 'newpvalue-biogrid', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        biogrid_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]

    file1 = open(result_address + 'newpvalue-string-exp', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        string_exp_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]

    file1 = open(result_address + 'newpvalue-string-rest', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        string_rest_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]

    file1 = open(result_address + 'newpvalue-sifts_accession', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        sifts_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]

    file1 = open(result_address + 'newpvalue-hprd', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        hprd_pvalue[(line_sp[0], line_sp[1])] = line_sp[3]

    result = open(result_address + 'newpvalue-all', 'w')

    i=0
    errors = 0
    N=len(intact_pvalue)
    print('len of intact_pvalue file:', N)
    for item in intact_pvalue:
        i+= 1
        if i%8000==0: print(round(100*i/N, 2))
        try: 
          result.write(
            item[0] + '\t' + item[1] + '\t' + caps_score[item] + '\t' + intact_pvalue[item] + '\t' + dip_pvalue[item] + '\t' +
              mint_pvalue[item] + '\t' + hprd_pvalue[item] + '\t' + biogrid_pvalue[item] + '\t' + string_exp_pvalue[item] + '\t' +
              string_rest_pvalue[item] + '\t' + sifts_pvalue[item] + '\n')
        except: errors+=1
    result.close()
    print('Number of errors:', errors, 'out of', N)

    
### Function gold_silver_bronze ###############################################

def round_scores(x):
    if float(x)==0 : return 0
    return f"{float(x):.8f}"

def gold_silver_bronze():
    # this function writes on one file: 'result-all'
    gs = set()
    gs_file= open(source_address + 'pfam-pfam-interaction-goldstandard', 'r')
    for line in gs_file:
        line_sp = line.rstrip().split("\t")
        gs.add((line_sp[0], line_sp[1]))
        gs.add((line_sp[1], line_sp[0]))


    calculated_dict = {}
    lenght = 0
    calculated = open(source_address + ppi, 'r')
    for line in calculated:
        lenght += 1
        line_sp = line.rstrip().split("\t")
        calculated_dict[(line_sp[0], line_sp[1])] = dict()
        calculated_dict[(line_sp[0], line_sp[1])]['caps'] = line_sp[10]
        calculated_dict[(line_sp[0], line_sp[1])]['intact'] = line_sp[2]
        calculated_dict[(line_sp[0], line_sp[1])]['dip'] = line_sp[3]
        calculated_dict[(line_sp[0], line_sp[1])]['mint'] = line_sp[4]
        calculated_dict[(line_sp[0], line_sp[1])]['biogrid'] = line_sp[5]
        calculated_dict[(line_sp[0], line_sp[1])]['string_exp'] = line_sp[6]
        calculated_dict[(line_sp[0], line_sp[1])]['string_rest'] = line_sp[7]
        calculated_dict[(line_sp[0], line_sp[1])]['sifts_acc'] = line_sp[8]
        calculated_dict[(line_sp[0], line_sp[1])]['hprd'] = line_sp[9]

    lenght = 84552  # additional contrainte !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    result = open(result_address + 'p_value_classified_ppidm_octobre2020', 'w')
    result.write("D1\tD2\tSCORE\tINTACT_SCORE\tINTACT_PV\tDIP_SCORE\tDIP_PV\tMINT_SCORE\tMINT_PV\tHPRD_SCORE\tHPRD_PV\tBIOGRID_SCORE\tBIOGRID_PV\tSTRING_EXP_SCORE\tSTRING_EXP_PV\tSTRING_REST_SCORE\tSTRING_REST_PV\tSIFTS_SCORE\tSIFTS_PV\tCLASS\tINTERPRO\n")
    pv = open(result_address + 'newpvalue-all', 'r')
    d_gold_distinct = set()
    d_silver_distinct = set()
    d_bronze_distinct = set()
    for line in pv:
        line_sp = line.rstrip().split("\t")
        d1 = line_sp[0]
        d2 = line_sp[1]
        gs_flag= 'No'
        if (d1, d2) in gs:
            gs_flag = 'Yes'
        caps_score = line_sp[2]
        if caps_score != calculated_dict[(d1, d2)]['caps']:
            print("Something Wrong!")
            print(d1, d2, caps_score, calculated_dict[(d1, d2)]['caps'])
            exit()
        intact = line_sp[3]
        dip = line_sp[4]
        mint = line_sp[5]
        hprd = line_sp[6]
        biogrid = line_sp[7]
        string_exp = line_sp[8]
        string_rest = line_sp[9]
        sifts = line_sp[10]
        critical_val = 0.05 / lenght

        how_many_significant = 0

        #if intact != 'NA' :
        if  float( round_scores( calculated_dict[(d1, d2)]['intact'])) != 0.0:
            if intact != '1*' and intact != 'NA' and float(intact) <= critical_val:
                how_many_significant += 1

        #if dip != 'NA':
        if  float( round_scores( calculated_dict[(d1, d2)]['dip'])) != 0.0:
            if dip != '1*' and dip != 'NA' and float(dip) <= critical_val:
                how_many_significant += 1

        #if mint != 'NA':
        if  float( round_scores( calculated_dict[(d1, d2)]['mint'])) != 0.0:
            if mint != '1*' and mint != 'NA' and float(mint) <= critical_val:
                how_many_significant += 1

        #if hprd != 'NA':
        if  float( round_scores( calculated_dict[(d1, d2)]['hprd'])) != 0.0:
            if hprd != '1*' and hprd != 'NA' and float(hprd) <= critical_val:
                how_many_significant += 1

        #if biogrid != 'NA':
        if  float( round_scores( calculated_dict[(d1, d2)]['biogrid'])) != 0.0:
            if biogrid != '1*' and biogrid != 'NA' and float(biogrid) <= critical_val:
                how_many_significant += 1

        #if string_exp != 'NA':
        if  float( round_scores( calculated_dict[(d1, d2)]['string_exp'])) != 0.0:
            if string_exp != '1*' and string_exp != 'NA' and float(string_exp) <= critical_val:
                how_many_significant += 1

        #if string_rest != 'NA':
        if  float( round_scores( calculated_dict[(d1, d2)]['string_rest'])) != 0.0:
            if string_rest != '1*' and string_rest != 'NA' and float(string_rest) <= critical_val:
                how_many_significant += 1

        #if sifts != 'NA':
        if  float( round_scores( calculated_dict[(d1, d2)]['sifts_acc'])) != 0.0:
            if sifts != '1*' and sifts != 'NA' and float(sifts) <= critical_val:
                how_many_significant += 1

        # if d1 == 'PF01123' and d2 =='PF07654':
        #     print(how_many, how_many_significant)
        if how_many_significant >= 4 :
            result.write(
                d1 + "\t" + d2 + "\t" + caps_score + "\t" + calculated_dict[(d1, d2)]['intact'] + "\t" + intact + "\t" +
                calculated_dict[(d1, d2)]['dip'] + "\t" + dip + "\t" + calculated_dict[(d1, d2)][
                    'mint'] + "\t" + mint + "\t" + calculated_dict[(d1, d2)]['hprd'] + "\t" + hprd + "\t" +
                calculated_dict[(d1, d2)]['biogrid'] + "\t" + biogrid + "\t" + calculated_dict[(d1, d2)][
                    'string_exp'] + "\t" + string_exp + "\t" + calculated_dict[(d1, d2)][
                    'string_rest'] + "\t" + string_rest + "\t" + calculated_dict[(d1, d2)][
                    'sifts_acc'] + "\t" + sifts + "\t" + "Gold\t" + gs_flag + "\n")
            d_gold_distinct.add(d1)
            d_gold_distinct.add(d2)
        elif how_many_significant < 4 and 0 < how_many_significant:
            result.write(
                d1 + "\t" + d2 + "\t" + caps_score + "\t" + calculated_dict[(d1, d2)]['intact'] + "\t" + intact + "\t" +
                calculated_dict[(d1, d2)]['dip'] + "\t" + dip + "\t" + calculated_dict[(d1, d2)][
                    'mint'] + "\t" + mint + "\t" + calculated_dict[(d1, d2)]['hprd'] + "\t" + hprd + "\t" +
                calculated_dict[(d1, d2)]['biogrid'] + "\t" + biogrid + "\t" + calculated_dict[(d1, d2)][
                    'string_exp'] + "\t" + string_exp + "\t" + calculated_dict[(d1, d2)][
                    'string_rest'] + "\t" + string_rest + "\t" + calculated_dict[(d1, d2)][
                    'sifts_acc'] + "\t" + sifts + "\t" + "Silver\t" + gs_flag + "\n")
            d_silver_distinct.add(d1)
            d_silver_distinct.add(d2)
        else:
            result.write(
                d1 + "\t" + d2 + "\t" + caps_score + "\t" + calculated_dict[(d1, d2)]['intact'] + "\t" + intact + "\t" +
                calculated_dict[(d1, d2)]['dip'] + "\t" + dip + "\t" + calculated_dict[(d1, d2)][
                    'mint'] + "\t" + mint + "\t" + calculated_dict[(d1, d2)]['hprd'] + "\t" + hprd + "\t" +
                calculated_dict[(d1, d2)]['biogrid'] + "\t" + biogrid + "\t" + calculated_dict[(d1, d2)][
                    'string_exp'] + "\t" + string_exp + "\t" + calculated_dict[(d1, d2)][
                    'string_rest'] + "\t" + string_rest + "\t" + calculated_dict[(d1, d2)][
                    'sifts_acc'] + "\t" + sifts + "\t" + "Bronze\t" + gs_flag + "\n")
            d_bronze_distinct.add(d1)
            d_bronze_distinct.add(d2)

    result.close()

    print('distinct gold doms: ', d_gold_distinct)
    print('distinct silver doms: ', d_silver_distinct)
    print('distinct bronze doms: ', d_bronze_distinct)





##############################################################################
# Calculate p_value for each source, accumulate in one file, then classify DDIs
##############################################################################

etoile_lists_dic = {}

for source in sources: 
    print("Processing ",source,':')
    etoile_lists_dic[source] = pvalue_calculation(source)

with open(result_address+'etoile_lists_dic.pickle', 'wb') as handle:
    pickle.dump(etoile_lists_dic, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
accumulate_pvalues()
gold_silver_bronze()




