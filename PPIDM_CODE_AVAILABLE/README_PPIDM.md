# ppidm2020

PPIDM description : The PPIDM  (Protein-Protein Interactions Domain Miner) approach has been designed by Zia Alborzi during his PhD thesis in the Capsid team. The goal is to infer Domain-Domain interactions (DDIs) from multiple sources of PPIs. 
The approach is an extension of our previously described CODAC (Computational Discovery of Direct Associations using Common neighbors) method for inferring new edges in a tripartite graph. 
The PPIDM method has been applied to seven widely used PPI resources, using as Gold-Standard a set of DDIs extracted from 3D structural databases. Overall, PPIDM has produced a dataset of 84,552 non-redundant DDIs.

References : 
CODAC : Alborzi SZ, Ritchie DW, Devignes MD. Computational discovery of direct associations between GO terms and protein domains. BMC Bioinformatics. 2018;19-S(14):53-66.
PPIDM : Alborzi SZ, Nacer AA, Najjar H, Ritchie DW, Devignes MD. PPIDomainMiner : Inferring domain-domain interactions from multiple sources of protein-protein pnteractions. Accepted in PLOS Computational Biology. 2021. Available on BioRxiv as  https://doi.org/10.1101/2021.03.03.433732 

List of codes necessary to run PPIDM.
Contributors : Zia Alborzi, Amina Ahmed-Nacer, Hiba Najjar

Input data: prepared PPI files from various sources. Soon available on UL Dataverse.

Output data: csv file with headers : Domain 1, Domain 2, Consensus Score, [Score in each PPI source, P-Value in each PPI source] repeated, Category, In_Gold-Standard (yes/no). Available from http://ppidm.loria.fr

The code is divided in three parts according to the author and the step in the PPIDM process.

ppidmcodepart1 (Zia Alborzi) : Computation of the PPIDM consensus similarity matrix.
    For each PPI source, produce adjacency matrices, compute cosine similarity and p-values. Optimize the set of weights and compute the consensus similarity matrix. This part is constituted of python scripts

ppidmcodepart2 (Hiba Najjar) : Generating the PPIDM dataset and categorization.
    Determine the threshold, produce the final list of PPIs, assign to categories, compare with existing datasets (3did, domine). This part is constituted of python scripts embedded in jupyter notebooks

ppidmcodepart3 (Amina Ahmed nacer) : Evaluating the PPIDM dataset.
    Determining relevant DDIs for explaining PPIs. This part is consituted of java programs.



