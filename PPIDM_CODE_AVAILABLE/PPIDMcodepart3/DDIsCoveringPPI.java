import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Function which returns the set of ddis covering a ppi
 * 
 */
public class DDIsCoveringPPI {

	public static void main(String[] args) throws IOException {

		String ppiList = "PPIDM2020/GKBOX/PPICoveredbyDDI";
		String pfamOfpdb = "proteinPfam";
		List<String> pfamspdb0 = new ArrayList<String>(200); // can adjust depending of the maxiimum pfams for a pdb
		List<String> pfamspdb1 = new ArrayList<String>(200);
		List<String> pfamsPDB = new ArrayList<String>(20500);
		String line;
		String[] ppi;

		BufferedReader br = new BufferedReader(new FileReader(new File(pfamOfpdb + ".csv")));
		while ((line = br.readLine()) != null) {
			pfamsPDB.add(line);
		}
		br.close();

		BufferedReader br2 = new BufferedReader(new FileReader(new File(ppiList + ".txt")));
		while ((line = br2.readLine()) != null) {

			PrintWriter writeRes = new PrintWriter(
					new File("PPIDM2020/GKBOX/Res/" + line + "TouteslesDDISQuiPeuventCouvrircettePPIS.txt"), "UTF-8");

			String[] temp;
			ppi = line.split("\t");

			// verifier si la DDI fait partie des ddi qui couvre les PPIs à partir des
			// domaines pfam

			for (int i = 0; i < pfamsPDB.size(); i++) {
				temp = (pfamsPDB.get(i)).split(";");
				if (temp[0].equals(ppi[0]))
					for (int j = 0; j < temp.length; j++)
						pfamspdb0.add(temp[j]);
				if (temp[0].equals(ppi[1]))
					for (int j = 0; j < temp.length; j++)
						pfamspdb1.add(temp[j]);
			}

			for (int i = 1; i < pfamspdb0.size(); i++) {
				for (int j = 1; j < pfamspdb1.size(); j++) {

					writeRes.println(pfamspdb0.get(i) + ";" + pfamspdb1.get(j));

				}
			}
			writeRes.close();

		}
		br2.close();
	}

}

