import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Function which returns the set of relevant ddis: covering at least one PPI
 * 
 */

public class RelevantDDIs {
	public static void main(String[] args) throws IOException {

		String PFAM = "PPIDM2020/GKBOX/COMPLETEPFAM_ciombinaisons";
		String DDIs = "PPIDM2020/PPIDM2020"; 
		List<String> ddiBD = new ArrayList<String>(10000); 
		BufferedReader br = new BufferedReader(new FileReader(new File(DDIs + ".csv")));
		String line;
		String[] pfam;
		int k = 0;
		while ((line = br.readLine()) != null) {
			pfam = line.split(";"); 
			ddiBD.add(pfam[0] + ";" + pfam[1]); 
		}
		br.close();
		System.out.println("Step running : DDis intialized.");

		PrintWriter writeRes = new PrintWriter(new File("ImportantDDIs.txt"), "UTF-8");

		
		while (k < ddiBD.size()) {

			boolean trouve = false;

			BufferedReader br2 = new BufferedReader(new FileReader(new File(PFAM + ".txt")));
			while ((line = br2.readLine()) != null && !trouve) {

				// verifier si la DDI fait partie des ddi qui couvre les PPIs
				String[] temp2 = (ddiBD.get(k)).split(";");
				String[] PFAMM = line.split("\t");

				if ((temp2[0].equals(PFAMM[0]) && temp2[1].equals(PFAMM[1]))
						|| (temp2[0].equals(PFAMM[1]) && temp2[1].equals(PFAMM[0]))) {

					trouve = true;

					writeRes.println(temp2[0] + "\t" + temp2[1]);
				
				}

			}

			

			br2.close();
			k++;
		}

		writeRes.close();

	
	}

}

