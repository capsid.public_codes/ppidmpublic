import java.io.BufferedReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Function which returns the number of PPI for each relevant DDI
 * 
 */

public class NbPPIPerDDI {

	public static void main(String[] args) throws IOException {

		String ppiList = "PPIDM2020/GKBOX/PPICoveredbyDDI"; // set of
															// covered
															// PPIs
		String ddiList = "PPIDM2020/GKBOX/ImportantDDIs"; // relevant ddis
		String line;
		String line2;
		String line3;
		String[] ppi;

		int nb = 0;

		BufferedReader br = new BufferedReader(new FileReader(new File(ddiList + ".txt")));

		while ((line = br.readLine()) != null) {
			nb = 0;

			PrintWriter writeRes = new PrintWriter(new File("PPIDM2020/GKBOX/ResNbPPIPerDDIs/" + line + ".txt"),
					"UTF-8");

			ppi = line.split("\t");

			/// Pour chaque DDI: lire la liste de toutes les DDIs qui couvrent une PPI
			// et voir si elle en fait partie

			BufferedReader br2 = new BufferedReader(new FileReader(new File(ppiList + ".txt")));
			while ((line2 = br2.readLine()) != null) {

				/// lire le fichier comprenant les ddis qui couvrent la ppi en cours

				BufferedReader br3 = new BufferedReader(new FileReader(
						new File("PPIDM2020/GKBOX/Res/" + line2 + "TouteslesDDISQuiPeuventCouvrircettePPIS.txt")));
				while ((line3 = br3.readLine()) != null) {
					if (line3.equals(ppi[0] + ";" + ppi[1]) || line3.equals(ppi[1] + ";" + ppi[0])) {

						writeRes.println(line2);

						nb++;

					}
				}
				br3.close();
			}

			br2.close();

			writeRes.close();

		}
		br.close();

	}
}

