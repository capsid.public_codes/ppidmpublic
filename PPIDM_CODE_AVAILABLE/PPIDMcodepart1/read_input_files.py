from global_variables import *
from headers import *

def finilize_interaction(interaction_string):
    interaction = ''
    temp = interaction_string.lstrip().split(' ')
    for item in temp:
        if '-' in item:
            item_corrected = item.split('-')
            interaction = interaction + item_corrected[0]  + "\t"
        else:
            interaction = interaction + item + "\t"
    return interaction

def uniprot_tags_reader(Source):
    start = datetime.datetime.now()
    if Source == "tr":
        print("Reading %s dat file(around 3 hours)" % (Source))
        file1 = open(source_address + 'uniprot_trembl.dat', 'r')
        ReviewedOrUnreviewed = "Unreviewed;"
    elif Source == "sp":
        print("Reading %s dat file (around 1 min)" % (Source))
        file1 = open(source_address + 'uniprot_sprot.dat', 'r')
        ReviewedOrUnreviewed = "Reviewed;"

    result_pfam = open(result_address + 'pfam-seq-' + Source, 'w')
    result_tax = open(result_address + 'ncbitax-seq-' + Source, 'w')
    result_gene = open(result_address + 'gene-seq-' + Source, 'w')
    result_string = open(result_address + 'string-seq-' + Source, 'w')
    indexx = 1
    for line1 in file1:
        if "ID   " in line1 and ReviewedOrUnreviewed in line1:
            indexx += 1
            ACCESSION = ""
            string_id = ""
            gene_id = ""
            ncbi_tax = ""
            pfam = ""


            for line1 in file1:
                if "SQ   SEQUENCE" in line1:
                    break

                if line1.startswith("AC") and "AC   " in line1 and len(ACCESSION) == 0:
                    line1Split = re.split('   ', line1.rstrip())
                    ACCESSION = line1Split[1].split(";")


                if line1.startswith('GN   Name=') and gene_id == "":
                    line1Split = line1.split('   ')
                    line1SplitSplit = line1Split[1].split(';')
                    temp = line1SplitSplit[0][5:]
                    # print(temp)
                    # print(ACCESSION)
                    tempsp = temp.split(' ')
                    gene_id = tempsp[0]
                    # raw_input()
                    # if gene_id != 'STAT3':
                    #     continue

                if line1.startswith('OX') and ncbi_tax == "":
                    temp = line1.rstrip()[16:-1]
                    tempsp = temp.split(' ')
                    ncbi_tax = tempsp[0]

                if line1.startswith('DR'):
                    if 'STRING;' in line1 and string_id == "":
                        line1Split = line1.rstrip().split(';')
                        string_id = line1Split[1][1:]
                    if "Pfam;" in line1:
                        line1Split = line1.rstrip().split(';')
                        pfam = line1Split[1] + pfam


            if ACCESSION != "" and ncbi_tax != "":
                result_tax.write(ncbi_tax + '\t' + ACCESSION[0] + '\n')
                # print (ACCESSION)
                # print (gene_id)

            if ACCESSION != "" and gene_id != "":
                result_gene.write(gene_id + '\t' + ACCESSION[0]+ '\n')

            if ACCESSION != "" and string_id != "":
                result_string.write( string_id + '\t' + ACCESSION[0]+ '\n')

            if ACCESSION != "" and pfam != "":
                pfams = pfam.split(' ')
                pfams = filter(None, pfams)
                for pfam in pfams:
                    result_pfam.write(pfam + '\t' +  ACCESSION[0]+ '\n')

    result_pfam.close()
    result_tax.close()
    result_gene.close()
    result_string.close()
    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")



def intact_reader():
    start = datetime.datetime.now()

    result = open(result_address + 'source1_intact', 'w')
    file1 = open(source_address + 'intact.txt', 'r')
    counter = 0
    file1.readline()
    associations = set()
    for line in file1:
        line_sp = line.split('\t')
        if line_sp[0].startswith('uniprotkb') and line_sp[1].startswith('uniprotkb'):
            counter += 1
            accession1 = line_sp[0][10:]
            accession2 = line_sp[1][10:]
            if '-' in accession1:
                temp = accession1.split('-')
                accession1 = temp[0]
            if '-' in accession2:
                temp = accession2.split('-')
                accession2 = temp[0]

            if accession1 < accession2:
                associations.add((accession1, accession2))
            else:
                associations.add((accession2, accession1))

    for (item1,item2) in associations:
        result.write(item1 + '\t' + item2 + "\n")
    print(counter)
    result.close()

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")


def mint_reader():
    start = datetime.datetime.now()

    result = open(result_address + 'source2_mint', 'w')
    file1 = open(source_address + 'MINT_MiTab.txt', 'r')
    counter = 0

    associations = set()
    for line in file1:
        line_sp = line.split('\t')
        if line_sp[0].startswith('uniprotkb') and line_sp[1].startswith('uniprotkb'):
            counter += 1
            accession1 = line_sp[0][10:]
            accession2 = line_sp[1][10:]
            if '-' in accession1:
                temp = accession1.split('-')
                accession1 = temp[0]
            if '-' in accession2:
                temp = accession2.split('-')
                accession2 = temp[0]

            if accession1 < accession2:
                associations.add((accession1, accession2))
            else:
                associations.add((accession2, accession1))

    for (item1,item2) in associations:
        result.write(item1 + '\t' + item2 + "\n")
    print(counter)
    result.close()

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")


def dip_reader():
    start = datetime.datetime.now()

    result = open(result_address + 'source3_dip', 'w')
    file1 = open(source_address + 'dip.txt', 'r')
    counter = 0
    file1.readline()
    associations = set()
    for line in file1:
        line_sp = line.split('\t')
        if 'uniprotkb' in line_sp[0] and 'uniprotkb' in line_sp[1]:
            counter += 1
            accession1 = line_sp[0][10:]
            temp = accession1.split(':')
            accession1 = temp[len(temp) - 1]
            accession2 = line_sp[1][10:]
            temp = accession2.split(':')
            accession2 = temp[len(temp) - 1]

            if '-' in accession1:
                temp = accession1.split('-')
                accession1 = temp[0]
            if '-' in accession2:
                temp = accession2.split('-')
                accession2 = temp[0]

            if accession1 < accession2:
                associations.add((accession1, accession2))
            else:
                associations.add((accession2, accession1))

    for (item1,item2) in associations:
        result.write(item1 + '\t' + item2 + "\n")
    print(counter)
    result.close()

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")


def biogrid_reader():
    start = datetime.datetime.now()

    gene_seq_dict = dict()
    file1 = open(result_address + 'gene-seq-sp', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        if line_sp[0] in gene_seq_dict:
            gene_seq_dict[line_sp[0]].add(line_sp[1])
        else:
            gene_seq_dict[line_sp[0]] = set()
            gene_seq_dict[line_sp[0]].add(line_sp[1])

    seq_tax_dict = dict()
    file1 = open(result_address + 'ncbitax-seq-sp', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        seq_tax_dict[line_sp[1]] = line_sp[0]

    result = open(result_address + 'source4_biogrid', 'w')
    file1 = open(source_address + 'biogrid.txt', 'r')
    counter = 0
    for i in range(36):
        file1.readline()
    associations = set()

    for line in file1:
        line_sp = line.rstrip().split('\t')
        g1 = line_sp[2]
        g2 = line_sp[3]
        tax1 = line_sp[9]
        tax2 = line_sp[10]

        # print('-----------')
        # print(tax1)
        # print(gene_seq_dict['STAT3'])

        if g1 in gene_seq_dict:
            for item1 in gene_seq_dict[g1]:
                # print('----- ' + seq_tax_dict[item1])
                if tax1 == seq_tax_dict[item1]:
                    if g2 in gene_seq_dict:
                        for item2 in gene_seq_dict[g2]:
                            # print('////// ' + seq_tax_dict[item2])
                            if tax2 == seq_tax_dict[item2]:
                                counter += 1
                                if item1 < item2:
                                    associations.add((item1, item2))
                                else:
                                    associations.add((item2, item1))

    for (item1,item2) in associations:
        result.write(item1 + '\t' + item2 + "\n")
    print(counter)
    result.close()

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")



def string_reader():
    start = datetime.datetime.now()

    string_seq_dict = dict()
    file1 = open(result_address + 'string-seq-sp', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        if line_sp[0] in string_seq_dict:
            string_seq_dict[line_sp[0]].add(line_sp[1])
        else:
            string_seq_dict[line_sp[0]] = set()
            string_seq_dict[line_sp[0]].add(line_sp[1])

    result1 = open(result_address + 'source5_string-exp', 'w')
    result2 = open(result_address + 'source5_string-rest', 'w')
    file1 = open(source_address + 'protein.links.detailed.v10.txt', 'r')
    counter = 0
    associations_expt = set()
    associations_rest = set()

    file1.readline()
    for line in file1:
        line_sp = line.rstrip().split(' ')
        # if int(line_sp[2]) < 500:
        #     continue
        counter += 1
        # if counter == 500000:
        #     break
        print(counter)
        string1 = line_sp[0]
        string2 = line_sp[1]
        exp_score = int(line_sp[6])
        rest_score = int(line_sp[9]) - exp_score
        # print(line_sp)
        # print(exp_score)
        # print(rest_score)
        # exit()
        list_acc1 = []
        list_acc2 = []
        if string1 in string_seq_dict:
            list_acc1 = string_seq_dict[string1]
        if string2 in string_seq_dict:
            list_acc2 = string_seq_dict[string2]

        for item1 in list_acc1:
            for item2 in list_acc2:
                if item1 < item2:
                    if exp_score != 0:
                        associations_expt.add((item1, item2))
                    if rest_score != 0:
                        associations_rest.add((item1, item2))
                else:
                    if exp_score != 0:
                        associations_expt.add((item2, item1))
                    if rest_score != 0:
                        associations_expt.add((item2, item1))



    for (item1,item2) in associations_expt:
        result1.write(item1 + '\t' + item2 + "\n")
    result1.close()

    for (item1,item2) in associations_rest:
        result2.write(item1 + '\t' + item2 + "\n")
    result2.close()

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")

def sifts_accession_reader():
    start = datetime.datetime.now()

    associations_acc = set()
    associations_pdb = set()
    accpted_ppi = sifts_accession_reader_filter_noninteractingChains()
    result = open(result_address + 'source6_sifts_accession', 'w')
    file1 = open(source_address + 'pdb_chain_uniprot.tsv', 'r')
    file2 = open(source_address + 'pdb_chain_pfam.tsv', 'r')
    file1.readline()
    file1.readline()

    pdb_chain_seq = dict()
    for line in file1:
        line_sp = line.rstrip().split('\t')
        pdb = line_sp[0].rstrip()
        pdb = line_sp[0].lstrip()
        chain = line_sp[1].rstrip()
        chain = line_sp[1].lstrip()
        seq = line_sp[2].rstrip()
        seq = line_sp[2].lstrip()
        if pdb in pdb_chain_seq:
            if chain in pdb_chain_seq[pdb]:
                pdb_chain_seq[pdb][chain].append(seq)
            else:
                pdb_chain_seq[pdb][chain] = list()
                pdb_chain_seq[pdb][chain].append(seq)
        else:
            pdb_chain_seq[pdb] = dict()
            pdb_chain_seq[pdb][chain] = list()
            pdb_chain_seq[pdb][chain].append(seq)

    file2.readline()
    file2.readline()
    for line in file2:
        line_sp = line.rstrip().split('\t')
        pdb = line_sp[0].rstrip()
        pdb = line_sp[0].lstrip()
        chain = line_sp[1].rstrip()
        chain = line_sp[1].lstrip()
        seq = line_sp[2].rstrip()
        seq = line_sp[2].lstrip()
        if pdb in pdb_chain_seq:
            if chain in pdb_chain_seq[pdb]:
                pdb_chain_seq[pdb][chain].append(seq)
            else:
                pdb_chain_seq[pdb][chain] = list()
                pdb_chain_seq[pdb][chain].append(seq)
        else:
            pdb_chain_seq[pdb] = dict()
            pdb_chain_seq[pdb][chain] = list()
            pdb_chain_seq[pdb][chain].append(seq)


    for pdb in pdb_chain_seq:
        chain_list = pdb_chain_seq[pdb]
        if len(chain_list) == 1:
            continue
        # used = set()
        for chain1 in chain_list:
            for chain2 in chain_list:
                if (pdb+chain1, pdb+chain2) not in accpted_ppi and (pdb+chain2, pdb+chain1) not in accpted_ppi:
                    continue
                if chain1 < chain2:
                    associations_pdb.add((pdb + '_' + chain1,pdb + '_' + chain2))
                # if (chain1, chain2) in used or (chain2, chain1) in used:
                #     continue
                # used.add((chain1, chain2))
                usedseq = set()
                for seq1 in pdb_chain_seq[pdb][chain1]:
                    for seq2 in pdb_chain_seq[pdb][chain2]:
                        if (seq1, seq2) in usedseq or (seq2, seq1) in usedseq:
                            continue
                        usedseq.add((seq1, seq2))
                        if seq1 < seq2:
                            associations_acc.add((seq1, seq2))
                        else:
                            associations_acc.add((seq2, seq1))

    for (item1,item2) in associations_acc:
        result.write(item1 + '\t' + item2 + "\n")
        # result.write(item2 + '\t' + item1 + "\n")

    for (item1,item2) in associations_pdb:
        result.write(item1 + '\t' + item2 + "\n")
    result.close()

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")

def sifts_accession_reader_filter_noninteractingChains():
    # result = open(result_address + 'source6_sifts_accession', 'r')
    d = '/KBDOCK_FOLDER_DATA/sup_ddi_inter_hetero'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    accepted_PPI = set()
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            chain2 = f_sp[6]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))

    d = '/KBDOCK_FOLDER_DATA/sup_ddi_inter_homo'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    # print(len(dirs))
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            chain2 = f_sp[6]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))

    d = '/KBDOCK_FOLDER_DATA/sup_ddi_intra_hetero'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    # print(len(dirs))
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            chain2 = f_sp[6]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))

    d = '/KBDOCK_FOLDER_DATA/sup_ddi_intra_homo'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    # print(len(dirs))
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            chain2 = f_sp[6]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))


    d = '/KBDOCK_FOLDER_DATA/sup_dpi_inter_hetero'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    # print(len(dirs))
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            chain2 = f_sp[6]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))

    d = '/KBDOCK_FOLDER_DATA/sup_dpi_intra_hetero'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    # print(len(dirs))
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            chain2 = f_sp[6]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))

    d = '/KBDOCK_FOLDER_DATA/sup_dspi_inter_hetero'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    # print(len(dirs))
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            temp = f_sp[5]
            temp_sp = temp.split(".")
            chain2 = temp_sp[0]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))

    d = '/KBDOCK_FOLDER_DATA/sup_dlpi_inter_hetero'
    dirs = [os.path.join(d, o) for o in os.listdir(d) if os.path.isdir(os.path.join(d, o))]
    # print(len(dirs))
    for dir in dirs:
        files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
        for f in files:
            if '_' not in f:
                continue
            f_sp = f.split('_')
            pdb = f_sp[1]
            chain1 = f_sp[2]
            temp = f_sp[5]
            temp_sp = temp.split(".")
            chain2 = temp_sp[0]
            if ((pdb + chain2, pdb + chain1)) not in accepted_PPI:
                accepted_PPI.add((pdb + chain1, pdb + chain2))

    print(len(accepted_PPI))
    return(accepted_PPI)


def sifts_reader_process(source, domain):
    start = datetime.datetime.now()

    associations = set()

    result = open(result_address + domain + '-' + domain  + '-interaction-'+ source , 'w')
    file1 = open(source_address + 'pdb_chain_pfam.tsv', 'r')

    pdb_chain_pfam = dict()
    file1.readline()
    file1.readline()
    for line in file1:
        line_sp = line.rstrip().split('\t')
        pdb = line_sp[0].rstrip()
        pdb = line_sp[0].lstrip()
        chain = line_sp[1].rstrip()
        chain = line_sp[1].lstrip()
        pfam = line_sp[3].rstrip()
        pfam = line_sp[3].lstrip()
        if pdb in pdb_chain_pfam:
            if chain in pdb_chain_pfam[pdb]:
                pdb_chain_pfam[pdb][chain].append(pfam)
            else:
                pdb_chain_pfam[pdb][chain] = list()
                pdb_chain_pfam[pdb][chain].append(pfam)
        else:
            pdb_chain_pfam[pdb] = dict()
            pdb_chain_pfam[pdb][chain] = list()
            pdb_chain_pfam[pdb][chain].append(pfam)


    for pdb in pdb_chain_pfam:
        chain_list = pdb_chain_pfam[pdb]
        if len(chain_list) == 1:
            continue
        used = set()
        for chain1 in chain_list:
            for chain2 in chain_list:
                if (chain1, chain2) in used or (chain2, chain1) in used:
                    continue
                used.add((chain1, chain2))
                usedpfam = set()
                for pf1 in pdb_chain_pfam[pdb][chain1]:
                    for pf2 in pdb_chain_pfam[pdb][chain2]:
                        if (pf1, pf2) in usedpfam or (pf2, pf1) in usedpfam:
                            continue
                            usedpfam.add((pf1, pf2))
                        if pf1 < pf2:
                            associations.add((pf1, pf2))
                        else:
                            associations.add((pf2, pf1))

    for (item1, item2) in associations:
        result.write(item1 + '\t' + item2 + "\n")
    result.close()

    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")



def hprd_reader():
    start = datetime.datetime.now()

    gene_seq_dict = dict()
    file1 = open(result_address + 'gene-seq-sp', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        if line_sp[0] in gene_seq_dict:
            gene_seq_dict[line_sp[0]].add(line_sp[1])
        else:
            gene_seq_dict[line_sp[0]] = set()
            gene_seq_dict[line_sp[0]].add(line_sp[1])

    seq_tax_dict = dict()
    file1 = open(result_address + 'ncbitax-seq-sp', 'r')
    for line in file1:
        line_sp = line.rstrip().split('\t')
        seq_tax_dict[line_sp[1]] = line_sp[0]

    result = open(result_address + 'source7_hprd', 'w')
    file1 = open(source_address + 'HPRD', 'r')
    counter = 0
    associations = set()

    for line in file1:
        line_sp = line.rstrip().split('\t')
        g1 = line_sp[0]
        g2 = line_sp[3]
        tax1 = '9606'
        tax2 = '9606'

        if g1 in gene_seq_dict:
            for item1 in gene_seq_dict[g1]:
                if tax1 == seq_tax_dict[item1]:
                    if g2 in gene_seq_dict:
                        for item2 in gene_seq_dict[g2]:
                            if tax2 == seq_tax_dict[item2]:
                                counter += 1
                                if item1 < item2:
                                    associations.add((item1, item2))
                                else:
                                    associations.add((item2, item1))

    print(len(associations))
    for (item1, item2) in associations:
        result.write(item1 + '\t' + item2 + "\n")
    print(counter)
    result.close()


    end = datetime.datetime.now()
    print("Running Time: " + str(end - start) + "\n")