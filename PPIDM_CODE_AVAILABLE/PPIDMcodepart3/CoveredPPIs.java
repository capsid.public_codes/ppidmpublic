import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Function which returns the set of covered PPI by PPIDM
 * 
 */
public class CoveredPPIs {
	public static void main(String[] args) throws IOException {

		String ppiList = "PPIHavingPfam";
		String pfamOfpdb = "proteinPfam";
		String bd4score = "PPIDM2020";
		List<String> ddiBD = new ArrayList<String>(10000);
		List<String> pfamsPDB = new ArrayList<String>(20500);
		List<String> pfamspdb0 = new ArrayList<String>(200);
		List<String> pfamspdb1 = new ArrayList<String>(200);

		BufferedReader br = new BufferedReader(new FileReader(new File(bd4score + ".csv")));
		String line;
		String[] pfam;
		String[] temp;
		String[] temp2;
		int res = 0;

		// Ajouter les DDIs dans un table
		while ((line = br.readLine()) != null) {
			pfam = line.split(";");
			ddiBD.add(pfam[0] + ";" + pfam[1]);
		}
		br.close();

		BufferedReader br2 = new BufferedReader(new FileReader(new File(pfamOfpdb + ".csv")));
		while ((line = br2.readLine()) != null) {
			pfamsPDB.add(line);
		}
		br2.close();

		String[] ppi;

		BufferedReader br3 = new BufferedReader(new FileReader(new File(ppiList + ".txt")));
		PrintWriter writeRes = new PrintWriter(new File("PPICoveredbyDDI.txt"), "UTF-8");

		int nb = 0;
		while ((line = br3.readLine()) != null) {
			nb++;

			ppi = line.split("\t");

			// ensemble des domaines pfam des ppi///
			for (int i = 0; i < pfamsPDB.size(); i++) {
				temp2 = (pfamsPDB.get(i)).split(";");

				if (temp2[0].equals(ppi[0]))
					for (int j = 0; j < temp2.length; j++)
						pfamspdb0.add(temp2[j]);
				if (temp2[0].equals(ppi[1]))
					for (int j = 0; j < temp2.length; j++)
						pfamspdb1.add(temp2[j]);
			}

			boolean trouve = false;

			/// verifier si au moins une combinaison des domaines pfam des ppi est une DDI
			for (int i = 1; i < pfamspdb0.size(); i++) {
				for (int j = 1; j < pfamspdb1.size(); j++) {

					int k = 0;
					while (k < ddiBD.size() && !trouve) {

						String[] temp3 = (ddiBD.get(k)).split(";");
						k++;

						if ((temp3[0].equals(pfamspdb0.get(i)) && temp3[1].equals(pfamspdb1.get(j)))
								|| (temp3[0].equals(pfamspdb1.get(j)) && temp3[1].equals(pfamspdb0.get(i)))) {

							trouve = true;
							writeRes.println(line);
							res++;
						}
					}
					if (trouve == true)
						break;
				}
				if (trouve == true)
					break;

			}

			pfamspdb0.clear();
			pfamspdb1.clear();

		}
		writeRes.close();

		br3.close();

	}

}

