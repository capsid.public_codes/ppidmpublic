# PPIDM generation and categorization, overlap with other DDI datasets.
This README file explains the jupyter notebooks for PPIDM code Part2
Author : Hiba Najjar; 2nd Year Student at the Ecole des Mines, Nancy.
co-author of publication ref https://biorxiv.org/cgi/content/short/2021.03.03.433732 

**PPIDM generation version august 2020":** 

--> generation of PPIDM corpus from the consensus matrix produced with Zia's code.
This code (Jupyter notebook  + associated html file) correspond to the optimal threshold determination step, using Glod-Standard positive DDIs and a set of negative DDIs generated using a shuffling technique


** p_value calculation":** 

--> calculate p_value of each DDI and classify full ppidm corpus.
This code is used to calculate p-values of each DDI relative to each PPI source.
The classification into Gold, Silver and Bronze categories has been re-written in the following code.


** Classification analysis": PPIDM (84552) Classification analysis** 

--> classify PPIDM corpus and analyse the results.
This code (Jupyter notebook  + associated html file) classifies the PPIDM corpus in three categories Gold, Siver and Bronze after some cleaning of the values (scores rounded to 8 decimals ; p-values labelled as "1\*" by the preceding code (meaning greater than 1) are set to 1 to allow comparison for classification. 
Analysis consist in classifyng into the 3 categories the HomoDDIs from PPIDM and 3did2020. 


**Overlap analysis with 3did, domine and instruct": PPIDM** 

--> Overlap analysis, with 3did, DOMINE and INstruct databases.
This code (Jupyter notebook + associated HTML file) computes the overlap between various datasets of DDIs to produce the results shown in the paper.
PPIDomainMiner : Inferring domain-domain interactions from
multiple sources of protein-protein interactions
Alborzi Seyed Ziaeddine, Ahmed Nacer Amina, Najjar Hiba, Ritchie David W and Devignes Marie-Dominique
Accepted in PLOS Computational Biology.
