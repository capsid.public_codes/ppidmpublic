# Evaluatinf the PPIDM dataset
This README file explains the role of each java code for PPIDMcodePart3
Written by Amina Ahmed-Nacer, coauthor of publication ref https://biorxiv.org/cgi/content/short/2021.03.03.433732

Class CoveredPPIs --> takes as input the set of PPIs (GKBOX or Imex for example), the pfam set of proteins, and the DDIs database (PPIDM2020 for example). It returns the set of PPIs covered by DDIs of the input set.


Class RelevantDDIs--> takes as input the complete-pfam-combinaisons (derived from the set of covered PPIs) and the set of DDIs (ex: PPIDM2020). It returns the set of DDIS which cover at least one PPIs according to the pfam combinaisons


Class NbPPIPerDDI --> takes as input the set of covered PPIs and the set of relevant DDIs. It returns for each relevant DDI, the set of PPIs that it really covers


Class NBDDisCoveringAPPI--> takes as input the set of covered PPIs and retuns the set of DDis covering each PPI


Class NatureOfRelevantDDis--> takes as input the set of relevant DDis and the set of PPIDM including the classification of DDis. It returns the set of classified relevant DDis according to the input PPIDM


PPIwithPfam--> returns the set of ppis having a pfam 


