import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NatureOfRelevantDDis {

	public static void main(String[] args) throws IOException {

		String ppiList = "PPIDM2020/GKBOX/ImportantDDIs2";
		String bd4score = "PPIDM2020/PPIDM2020"; 
		List<String> ddiBD = new ArrayList<String>(10000);
		BufferedReader br = new BufferedReader(new FileReader(new File(ppiList + ".txt")));
		String line;
		String[] pfam;
		int gold = 0;
		int bronze = 0;
		int silver = 0;
		while ((line = br.readLine()) != null) {
			pfam = line.split("\t"); 
			ddiBD.add(pfam[0] + ";" + pfam[1]); 
		}
		br.close();
	

		///// rechercher la DDI ainsi que sa catégorie////////

		int k = 0;
		while (k < ddiBD.size()) {
			System.out.println("recherche de la DDI numero" + k);

			boolean trouve = false;

			BufferedReader br3 = new BufferedReader(new FileReader(new File(bd4score + ".csv")));
			while ((line = br3.readLine()) != null && !trouve) {

				// verifier si la DDI fait partie des ddi qui couvre les PPIs
				String[] temp2 = (ddiBD.get(k)).split(";");
				String[] PFAMM = line.split(";");

				if ((temp2[0].equals(PFAMM[0]) && temp2[1].equals(PFAMM[1]))
						|| (temp2[0].equals(PFAMM[1]) && temp2[1].equals(PFAMM[0]))) {
					trouve = true;
					if (PFAMM[2].equals("Gold"))
						gold++;
					if (PFAMM[2].equals("Silver"))
						silver++;
					if (PFAMM[2].equals("Bronze"))
						bronze++;

				}

			}
			k++;
			br3.close();
		}

		System.out.println("le nb de gold est" + gold);
		System.out.println("le nb de silver est" + silver);
		System.out.println("le nb de bronze est" + bronze);

	}

}

